terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "3.70.0"
    }
  }
}

### Provider for the Audit account
provider "aws" {
  region     = "eu-west-2"
}

data "aws_iam_roles" "roles" {}

output "arns" {
  value = [
    for parts in [for arn in data.aws_iam_roles.roles.arns : split("/", arn)] :
    format("%s/%s", parts[0], element(parts, length(parts) - 1))
  ]
}

